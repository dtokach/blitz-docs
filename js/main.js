$(document).ready(function() {
    $('.login.user').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    });

    $('.menu-content').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    });
});

$(document).mouseup(function (e){ // событие клика по веб-документу
    var div = $('.login.user'); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0) { // и не по его дочерним элементам
        $('.login.user').removeClass('active');
    }
});

$(document).mouseup(function (e){ // событие клика по веб-документу
    var div = $('.menu-content'); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0) { // и не по его дочерним элементам
        $('.menu-content').removeClass('active');
    }
});